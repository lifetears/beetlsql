package org.beetl.sql.mapper.internal;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.mapper.MapperInvoke;

import java.lang.reflect.Method;

/**
 * create time : 2017-04-27 16:08
 *
 * @author luoyizhu@gmail.com
 */
public class SingleAMI extends MapperInvoke {

    @Override
    public Object call(SQLManager sm, Class entityClass,  Method m, Object[] args) {
        return sm.single(entityClass, args[0]);
    }

}
